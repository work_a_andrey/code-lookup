<?php

use Illuminate\Database\Seeder;

class AccountsSeeder extends Seeder
{
    private static $text = 
        'Lorem ipsum dolor sit amet consectetur adipiscing elit. In molestie sem eu sollicitudin porttitor. Fusce augue neque, dignissim at sagittis quis, ultricies a mauris.' .
        'Etiam id condimentum libero, posuere gravida neque. Phasellus lobortis vel dui faucibus pellentesque. Cras mollis at orci id tincidunt.' .
        'Phasellus imperdiet neque aliquam, luctus ipsum in, imperdiet sem. Nulla placerat lacus fringilla est tristique vehicula. Mauris id ipsum vitae risus placerat sollicitudin. Curabitur dictum suscipit ligula.' .
        'Ut in ante ut quam cursus fermentum id vitae metus. In luctus dui lorem, in efficitur ex molestie nec. Praesent vulputate ante sed nunc convallis, ut euismod enim eleifend. Aenean mollis, dui sit amet venenatis egestas, risus enim fringilla purus, et tempor tellus turpis ut metus. Praesent quis pellentesque elit, a eleifend dolor.';
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $words = explode(' ', self::$text);
        
        for($i = 0; $i < 10; $i++) {
            \App\Account::create([
                'name' => ucfirst($words[rand(0, 30)]),
            ]);
        }
    }
}
