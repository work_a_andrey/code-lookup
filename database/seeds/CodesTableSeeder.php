<?php

use Illuminate\Database\Seeder;

class CodesTableSeeder extends Seeder
{

    private static $categories = ['Main', 'Tests', 'Public', 'Invoices'];

    private static $descriptions = [
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In molestie sem eu sollicitudin porttitor. Fusce augue neque, dignissim at sagittis quis, ultricies a mauris.',
        'Etiam id condimentum libero, posuere gravida neque. Phasellus lobortis vel dui faucibus pellentesque. Cras mollis at orci id tincidunt.',
        'Phasellus imperdiet neque aliquam, luctus ipsum in, imperdiet sem. Nulla placerat lacus fringilla est tristique vehicula. Mauris id ipsum vitae risus placerat sollicitudin. Curabitur dictum suscipit ligula.',
        'Ut in ante ut quam cursus fermentum id vitae metus. In luctus dui lorem, in efficitur ex molestie nec. Praesent vulputate ante sed nunc convallis, ut euismod enim eleifend. Aenean mollis, dui sit amet venenatis egestas, risus enim fringilla purus, et tempor tellus turpis ut metus. Praesent quis pellentesque elit, a eleifend dolor.'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 50; $i++) {
            \App\Code::create([
                'category' => self::$categories[rand(0, 3)],
                'description' => self::$descriptions[rand(0, 3)],
                'codes' => self::generateCodes(rand(1, 4)),
            ]);
        }
        \App\Code::create([
            'category' => 'Homeowner Policies',
            'description' => 'No plumbing coverage, Named Peril, ACV',
            'codes' => 'NLHOA',
        ]);
    }

    /**
     * @param $codesToGenerate
     * @return string
     */
    private static function generateCodes($codesToGenerate)
    {
        $codes = [];
        for($i = 1; $i <= $codesToGenerate; $i++) {
            $codes[] = str_random(rand(3,9));
        }
        return implode(PHP_EOL, $codes);
    }
}
