<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'email' => 'admin@email.com',
            'password' => \Illuminate\Support\Facades\Hash::make('password'),
            'is_admin' => 1
        ]);
    }
}
