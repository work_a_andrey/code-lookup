<?php

use Illuminate\Database\Seeder;

class DefaultUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'email' => 'user',
            'password' => \Illuminate\Support\Facades\Hash::make('super'),
            'is_admin' => 0
        ]);
    }
}
