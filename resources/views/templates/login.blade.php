@extends('layouts.basic')
@section('content')

    <div style="height: 50px;"></div>
    <h1 class="text-center login-title">Sign in to continue</h1>
    <div class="account-wall">
        <form class="form-signin" action="" method="post">
            @if($errors->any())
                <ul class="alert alert-danger" style="list-style-type: none">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif
            <input name="email" type="text" class="form-control" placeholder="Email" required autofocus>
            <input name="password" type="password" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">
                Sign in
            </button>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>

@stop