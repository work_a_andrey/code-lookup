<div class="row">
    <div class="col-lg-4">
        <form action="/admin/codes/" method="get">
            <div class="form-group">
                <input type="text" name="q" id="q" class="form-control" value="{{$q}}" placeholder="Search" style="display: inline-block; width: 66%;">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </form>
    </div>
</div>
