@extends('layouts.default')
@section('content')
    <h1 class="page-header">Update code description</h1>

    <div class="row">
        <div class="col-md-6">
            <form action="/admin/codes/{{$code->id}}/delete" method="post">
                <button type="submit" class="btn btn-danger btn-mini" onclick="if(confirm('Do you really want to delete this item?')) { return true;} else {return false;}">Delete</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <form action="/admin/codes/{{$code->id}}/update" method="post">
                @include('templates.admin.codes._form')
            </form>
        </div>
    </div>
@stop