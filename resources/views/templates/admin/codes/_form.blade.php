<div class="form-group">
    <label for="account_id">Account</label>
    <select name="account_id" id="account_id" class="form-control">
        @foreach(\App\Account::all() as $account)
            <option value="{{$account->id}}" @if($account->id == $code->account_id) selected @endif>
                {{$account->name}}
            </option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label for="codes">Codes</label>
    <textarea required name="codes" id="description" cols="30" rows="10" class="form-control">{{$code->codes}}</textarea>
</div>
<div class="form-group">
    <label for="description">Description</label>
    <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{$code->description}}</textarea>
</div>
<button type="submit" class="btn btn-primary">Submit</button>
<input type="hidden" name="_token" value="{{ csrf_token() }}">