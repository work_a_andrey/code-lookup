@extends('layouts.default')
@section('content')
    <h1 class="page-header">Create code description</h1>

    <div class="row">
        <div class="col-lg-6">
            <form action="/admin/codes/store" method="post">
                @include('templates.admin.codes._form')
            </form>
        </div>
    </div>
@stop