@extends('layouts.default')
@section('content')
    <h1 class="page-header">Codes @if($account)by account "{{$account}}"@endif</h1>

    @include('templates.admin.codes._search_form')

    <p><a href="/admin/codes/create" class="btn btn-primary">Add Code</a></p>

    {!! $codes->links() !!}
    <div class="panel panel-default">
        <!-- Default panel contents -->
        @if($account)
        <div class="panel-heading">
            <a href="/admin/codes/"><b>Show All Codes</b></a>
        </div>
        @endif

        <!-- Table -->
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Account</th>
                <th style="width:50%">Description</th>
                <th>Codes</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($codes as $code)
            <tr>
                <td>{{$code->id}}</td>
                <td>
                    @if($code->account)
                    <a href="/admin/codes/?account_id={{$code->account->id}}">{{$code->account->name}}</a>
                    @endif
                </td>
                <td><a href="/admin/codes/{{$code->id}}">{!!nl2br($code->shortDescription())!!}</a></td>
                <td>{!!nl2br($code->codes)!!}</td>
                <td>
                    <form action="/admin/codes/{{$code->id}}/delete" method="post">
                        <button type="submit" class="btn btn-danger btn-mini" onclick="if(confirm('Do you really want to delete this item?')) { return true;} else {return false;}">Delete</button>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {!! $codes->links() !!}
@stop