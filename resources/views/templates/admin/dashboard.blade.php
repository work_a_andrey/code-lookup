@extends('layouts.default')
@section('content')
    <h1 class="page-header">Dashboard</h1>

    <div class="row">
        <div class="col-md-10">
            <form action="" method="post">
                <div class="form-group">
                    <label for="description">Text</label>
                    <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
        </div>
    </div>
@stop