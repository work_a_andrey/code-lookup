@extends('layouts.default')
@section('content')
    <h1 class="page-header">Accounts</h1>

    <p><a href="/admin/accounts/create" class="btn btn-primary">Add Account</a></p>

    <div class="panel panel-default">
        <!-- Table -->
        <table class="table">
            <thead>
            <tr>
                <th style="width: 5%;">#</th>
                <th>Account</th>
                <th style="width: 15%;"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($accounts as $account)
            <tr>
                <td>{{$account->id}}</td>
                <td><a href="/admin/accounts/{{$account->id}}/edit">{{$account->name}}</a></td>
                <td>
                    <form action="/admin/accounts/{{$account->id}}/delete" method="post">
                        <button type="submit" class="btn btn-danger btn-mini" onclick="if(confirm('Do you really want to delete this item?')) { return true;} else {return false;}">Delete</button>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
