@extends('layouts.default')
@section('content')
    <h1 class="page-header">Create Account</h1>

    <div class="row">
        <div class="col-lg-6">
            <form action="/admin/accounts/store" method="post">
                @include('templates.admin.accounts._form')
            </form>
        </div>
    </div>
@stop