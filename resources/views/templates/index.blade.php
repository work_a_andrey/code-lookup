@extends('layouts.default')
@section('content')
    <h1 class="page-header">Analyze text</h1>

    <div class="row">
        <div class="col-md-10">
            <form action="" method="post">
                <div class="form-group">
                    <label for="account_id">Account</label>
                    <select name="account_id" id="account_id" class="form-control">
                        @foreach(\App\Account::all() as $account)
                            <option value="{{$account->id}}" @if($account->id == \Illuminate\Support\Facades\Input::get('account_id')) selected @endif>
                                {{$account->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="text">Text</label>
                    <textarea name="text" id="text" cols="30" rows="10" class="form-control">{{$text}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>

            @if(!empty($foundCodes))
            <h3>Found Codes</h3>
                @if(count($foundCodes))
                <p>
                    <button class="btn btn-copy btn-info" data-clipboard-action="copy" data-clipboard-target="#code">
                        Copy results
                    </button>
                </p>
                @endif
            <ul id="code">
                @foreach($foundCodes as $codeName => $code)
                <li>
                    {{$codeName}} - {{$code->description}}
                </li>
                @endforeach
            </ul>
            @else
                <h3>No Results</h3>
            @endif
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.5/clipboard.min.js"></script>
    <script type="text/javascript">
        var clipboard = new Clipboard('.btn-copy');

        clipboard.on('success', function (e) {
            e.clearSelection();
        });
    </script>
@stop