<?php

namespace App\Providers;

use App\Code;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Code::saving(function(Code $code) {
            $currentCodes = $code->codes();
            foreach($code->account->codes as $accountCode) {
                if(is_null($code->id)) {
                    foreach($currentCodes as $codeAcronym) {
                        if(in_array($codeAcronym, $accountCode->codes())) {
                            return false;
                        }
                    }
                }
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
