<?php

namespace App;

class CodeParser
{
    /**
     * @var string
     */
    private $text;

    /**
     * @var array|void
     */
    private $words = [];

    /**
     * @var array
     */
    private $availableCodes = [];

    /**
     * @var array
     */
    private $codesMap = [];

    /**
     * @var array
     */
    private $codes = [];

    /**
     * CodeParser constructor.
     * @param $text
     * @param $codes
     */
    public function __construct($text, $codes)
    {
        $this->text = $text;
        $this->words = $this->getTextWords();

        $this->codes = $codes;
        $this->prepareCodes();
    }

    /**
     * Found and return codes in words
     * @return array
     */
    public function run()
    {
        $foundCodes = [];
        $foundCodesNames = array_intersect($this->availableCodes, $this->words);
        $foundCodesNames = array_merge($foundCodesNames, $this->getCodesFromRawText());
        
        foreach($foundCodesNames as $codeName) {
            $foundCodes[$codeName] = $this->codesMap[$codeName];
        }

        return $foundCodes;
    }

    /**
     * @return array
     */
    private function getCodesFromRawText()
    {
        $foundCodes = [];
        foreach($this->availableCodes as $availableCode) {
            if($this->isTextContainsWord($this->text, $availableCode)) {
                $foundCodes[] = $availableCode;
            }
        }
        return $foundCodes;
    }

    /**
     * @param string $text
     * @param string $word
     * @return bool
     */
    private function isTextContainsWord($text, $word)
    {
        $regexp = preg_match('/\s/', $word)
            ? '#'.str_replace(' ', '\s', preg_quote($word, '#')).'#i'
            : '#\b' . preg_quote($word, '#') . '\b#i';
        return !!preg_match($regexp, $text);
    }

    /**
     * Prepare codes: collect all available codes names and generate map by codes names
     */
    private function prepareCodes()
    {
        foreach($this->codes as $code) {
            /** @var $code \App\Code */
            $codesNames = $code->codes();
            $this->availableCodes = array_merge($this->availableCodes, $codesNames);
            foreach($codesNames as $codeName) {
                $this->codesMap[$codeName] = $code;
            }
        }
        $this->availableCodes = array_unique($this->availableCodes);
    }

    /**
     * Split text to words
     * @return array
     */
    private function getTextWords()
    {
        return preg_split('/\W/i', $this->text, -1, PREG_SPLIT_NO_EMPTY);
    }
}