<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'codes';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    /**
     * @return array
     */
    public function codes()
    {
        return array_map(function($code) {
            return trim($code);
        }, explode(PHP_EOL, $this->codes));
    }

    /**
     * @return mixed|string
     */
    public function shortDescription()
    {
        return strlen($this->description) <= 150
            ? $this->description
            : substr($this->description, 0, 150) . '...';
    }
}
