<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class LoginController extends Controller
{
    public function index()
    {
        if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {
            return logged_user()->isAdmin()
                ? redirect()->to('/admin/codes')
                : redirect()->to('/');
        } else {
            return redirect()->back()->withErrors(['auth' => 'Wrong email or password']);
        }
    }
}