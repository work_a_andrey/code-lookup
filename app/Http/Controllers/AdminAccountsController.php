<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class AdminAccountsController extends Controller
{
    public function index()
    {
        $accounts = Account::all();
        return view('templates.admin.accounts.index', [
            'accounts' => $accounts,
        ]);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $account = new Account();
        $account->name = '';
        return view('templates.admin.accounts.create', [
            'account' => $account
        ]);
    }

    /**
     * @return mixed
     */
    public function store()
    {
        $account = new Account();
        $account->name = Input::get('name');
        $account->save();

        return redirect()
            ->to('/admin/accounts/')
            ->with('status', 'Account was successfully created');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return redirect('/admin/accounts/' . $id . '/edit');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $account = Account::findOrFail($id);
        return view('templates.admin.accounts.edit', [
            'account' => $account
        ]);
    }

    /**
     * @param $id
     */
    public function update($id)
    {
        $account = Account::findOrFail($id);
        $account->name = Input::get('name');
        $account->save();

        return redirect()
            ->to('/admin/accounts/' . $account->id)
            ->with('status', 'Account was successfully updated');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $account = Account::findOrFail($id);
        $account->forceDelete();

        return redirect()
            ->to('/admin/accounts/')
            ->with('status', 'Account was successfully deleted');
    }
}
