<?php

namespace App\Http\Controllers;

use App\Code;
use App\CodeParser;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class DashboardController extends Controller
{
    /**
     * @return mixed
     */
    public function analyze()
    {
        $codes = Code::where('account_id', '=', Input::get('account_id'))->get();
        $codeParser = new CodeParser(
            Input::get('text'), 
            $codes
        );
        $foundCodes = $codeParser->run();

        return view('templates.index', [
            'foundCodes' => $foundCodes,
            'text' => Input::get('text')
        ]);
    }
    
}
