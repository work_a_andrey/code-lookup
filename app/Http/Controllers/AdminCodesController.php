<?php

namespace App\Http\Controllers;

use App\Code;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;

class AdminCodesController extends Controller
{

    /**
     * Show all codes
     * @return mixed
     */
    public function index()
    {
        $accountId = Input::get('account_id', false);
        $q = Input::get('q', '');

        $codes = Code::orderBy('id', 'desc');
        if($accountId) {
            $codes->where('account_id', '=', $accountId);
        }

        if(strlen($q)) {
            $codes->where('codes', 'like', "%{$q}%");
        }

        $codes = $codes->paginate(30);

        if($codes->count() == 1 && !empty($q)) {
            return redirect()->to('/admin/codes/' . $codes[0]->id);
        }

        return view('templates.admin.codes.index', [
            'codes' => $codes,
            'account' => $accountId,
            'q' => $q
        ]);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $code = new Code();
        $code->codes = '';
        return view('templates.admin.codes.create', [
            'code' => $code
        ]);
    }

    /**
     * @return mixed
     */
    public function store()
    {
        $code = new Code();
        $code->account_id = Input::get('account_id');
        $code->description = Input::get('description');
        $code->codes = Input::get('codes');
        if($code->save()) {
            return redirect()
                ->to('/admin/codes/')
                ->with('status', 'Code was successfully created');
        } else {
            return redirect()
                ->to('/admin/codes/')
                ->with('status', 'This code already exists.');
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return redirect('/admin/codes/' . $id . '/edit');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $code = Code::findOrFail($id);
        return view('templates.admin.codes.edit', [
            'code' => $code
        ]);
    }

    /**
     * @param $id
     */
    public function update($id)
    {
        $code = Code::findOrFail($id);
        $code->account_id = Input::get('account_id');
        $code->description = Input::get('description');
        $code->codes = Input::get('codes');
        $save = $code->save();

        return redirect()
            ->to('/admin/codes/')
            ->with('status', 'Code was successfully updated');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $code = Code::findOrFail($id);
        $code->forceDelete();

        return redirect()
            ->to('/admin/codes/')
            ->with('status', 'Code was successfully deleted');
    }

}
