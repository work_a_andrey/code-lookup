<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['auth']], function()
{
    Route::get('/', function () {
        return view('templates.index', [
            'text' => ''
        ]);
    });

    Route::post('/', ['uses' => 'DashboardController@analyze']);
});

Route::group(['middleware' => ['guest']], function()
{
    Route::get('/login', function() {
        return view('templates.login');
    });
    Route::post('/login', ['uses' => 'LoginController@index']);
});

Route::group(['middleware' => ['admin']], function()
{
    Route::get('/admin', function() {
        return redirect('/admin/codes');
    });

    Route::resource('/admin/codes', 'AdminCodesController');
    Route::post('/admin/codes/store', 'AdminCodesController@store');
    Route::post('/admin/codes/{id}/update', 'AdminCodesController@update');
    Route::post('/admin/codes/{id}/delete', 'AdminCodesController@destroy');

    Route::resource('/admin/accounts', 'AdminAccountsController');
    Route::post('/admin/accounts/store', 'AdminAccountsController@store');
    Route::post('/admin/accounts/{id}/update', 'AdminAccountsController@update');
    Route::post('/admin/accounts/{id}/delete', 'AdminAccountsController@destroy');
});
